# Set the image
FROM node:latest
# Set working directory
WORKDIR /usr/src/app
# Copy all package.json and package-lock.json
COPY package*.json ./
# Install npm modules
RUN npm install
# COPY all dir and file in root dir
COPY . .
# Expose to port 3000
EXPOSE 3000
# Set the command line
CMD [ "node", "app.js" ]