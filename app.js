// Library
const express = require('express');
const app = express();

// Soute
app.get('/', (req,res) => {
    res.send('Hello World')
})

app.get('/contact', (req,res) => {
    res.send('Halaman Contact')
})

app.get('/about', (req,res) => {
    res.send('Halaman About')
})

// Server
app.listen(3000,() => {
    console.log('Server is running on port 3000')
})